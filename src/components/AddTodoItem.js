import React from 'react';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {Card} from 'material-ui/Card';
import TodoActions from '../actions/TodoActions';

const ENTER_KEY = 13;

class AddTodoItem extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        newtodo: ''
      }
  }

  handleChange(event) {
    this.setState({ newtodo: event.target.value });
  }

  handleAddTodo() {
    this.addTodo();
  }

  handleKeyDown(event) {
    if (event.keyCode == ENTER_KEY) {
        this.addTodo();
    }
  }

  addTodo() {
    TodoActions.addTodo({
      title: this.state.newtodo,
      isCompleted: false
    });

    this.setState({ newtodo: '' });
  }

  render() {
    const styles = {
      item: {
        position: 'relative',
        margin: 20,
        padding: 20
      },
      button: {
        position: 'absolute',
        right: 10,
        top: '50%',
        transform: 'translateY(-50%)'
      }
    };
    return (
      <Card className='add-item' style={styles.item}>
        <TextField
          id='title'
          hintText={'What has to be done?'}
          value={this.state.newtodo}
          onKeyDown={this.handleKeyDown.bind(this)}
					onChange={this.handleChange.bind(this)}
					autoFocus={true}
          fullWidth={true} />
        <FloatingActionButton
          mini={true}
          onClick={this.handleAddTodo.bind(this)}
          style={styles.button}>
          <ContentAdd />
        </FloatingActionButton>
      </Card>
    );
  }
}

export default AddTodoItem;
