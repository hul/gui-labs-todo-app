import React from 'react';
import {ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';

class Status extends React.Component {
  render() {
    const text = '0 items left';
    return (
      <ToolbarGroup>
        <ToolbarTitle text={text} />
      </ToolbarGroup>
    );
  }
}

export default Status;
