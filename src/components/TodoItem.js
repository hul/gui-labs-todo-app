import React from 'react';
import {ListItem} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentRemove from 'material-ui/svg-icons/content/remove';

class TodoItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isCompleted: props.data.isCompleted,
      title: props.data.title
    };
  }

  onChange(event) {
    this.setState({
      isCompleted: event.target.checked
    });
  }

  onRemove(event) {

  }

  render() {
    const styles = {
      item: {
        position: 'relative',
      },
      checkbox: {},
      button: {
        position: 'absolute',
        right: 10,
        top: 10,
      }
    };
    return (
      <ListItem className='todo-item' style={styles.item}>
        <Checkbox
          id={this.props.id}
          checked={this.state.isCompleted}
          onCheck={this.onChange.bind(this)}
          label={this.state.title}
          style={styles.checkbox}>
        </Checkbox>
        <FloatingActionButton
          mini={true}
          onClick={this.onRemove.bind(this)}
          style={styles.button}>
          <ContentRemove />
        </FloatingActionButton>
      </ListItem>
    );
  }
}

export default TodoItem;
