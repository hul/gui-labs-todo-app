import React from 'react';
import {CardHeader} from 'material-ui/Card';

class Header extends React.Component {
  render() {
    return (
      <CardHeader title='TODOS'></CardHeader>
    );
  }
}

export default Header;
