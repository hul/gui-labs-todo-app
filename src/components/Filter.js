import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import {ToolbarGroup} from 'material-ui/Toolbar';

class Filter extends React.Component {
  render() {
    return (
      <ToolbarGroup className='filter'>
        <FlatButton label='Completed' />
        <FlatButton label='Incompleted' />
        <FlatButton label='Done' />
      </ToolbarGroup>
    );
  }
}

export default Filter;
