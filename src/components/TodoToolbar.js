import React from 'react';
import TodoItem from './TodoItem';
import Status from './Status';
import Filter from './Filter';
import {Toolbar, ToolbarGroup, ToolbarSeparator} from 'material-ui/Toolbar';

class TodoToolbar extends React.Component {
  render() {

    return (
      <Toolbar className='toolbar'>
        <Status />
        <ToolbarSeparator />
        <Filter />
      </Toolbar>
    );
  }
}

export default TodoToolbar;
