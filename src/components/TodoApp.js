import {Card} from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React from 'react';

import TodoItemList from './TodoItemList';
import AddTodoItem from './AddTodoItem';
import TodoToolbar from './TodoToolbar';
import Header from './Header';

import TodoActions from '../actions/TodoActions';
import TodoStore from '../store/TodoStore';

class TodoApp extends React.Component {
  constructor() {
    super();

    this.onChange = this.onChange.bind(this);
    this.state = TodoStore.getState();
  }

  componentDidMount() {
    TodoStore.listen(this.onChange);
    TodoActions.fetchTodos();
  }

  componentWillUnmount() {
    TodoStore.unlisten(this.onChange);
  }

  onChange(state) {
    console.log(this, state);
    this.setState(state);
  }

  render() {
    if (this.state.errorMessage) {
      return (
        <div>Something is wrong</div>
      );
    }

    return (
      <MuiThemeProvider>
        <Card>
          <Header />
          <AddTodoItem />
          <TodoItemList todos={this.state.todos} />
          <TodoToolbar />
        </Card>
      </MuiThemeProvider>
    );
  }
}

export default TodoApp;
