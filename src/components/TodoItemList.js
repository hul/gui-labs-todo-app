import React from 'react';
import TodoItem from './TodoItem';
import {List, ListItem} from 'material-ui/List';

class TodoItemList extends React.Component {
  render() {
    var todos = this.props.todos.map((item, i) => {
      return <TodoItem key={i} data={item} id={i}/>
    });
    return (
      <List className='todo-item'>
        {todos}
      </List>
    );
  }
}

export default TodoItemList;
