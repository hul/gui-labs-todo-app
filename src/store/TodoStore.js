import alt from '../alt';
import TodoActions from '../actions/TodoActions'

class TodoStore {
  constructor() {
    this.todos = [];

    this.bindListeners({
      handleUpdateTodos: TodoActions.UPDATE_TODOS,
      handleFetchTodos: TodoActions.FETCH_TODOS,
      handleTodosFailed: TodoActions.TODOS_FAILED,
      handleAddTodo: TodoActions.ADD_TODO
    });
  }

  handleUpdateTodos(todos) {
    this.todos = todos;
  }

  handleFetchTodos() {
    // reset the array while we're fetching new locations so React can
    // be smart and render a spinner for us since the data is empty.
    this.todos = [];
  }

  handleTodosFailed(errorMessage) {
    this.errorMessage = errorMessage;
  }

  handleAddTodo(todo) {
    this.todos.push(todo);
    console.log(this.todos);
  }

}

export default alt.createStore(TodoStore, 'TodoStore');
