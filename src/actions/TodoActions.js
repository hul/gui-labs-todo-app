import alt from '../alt';
import TodoSource from '../data/TodoSource';

class TodoActions {
  updateTodos(todos) {
    return todos;
  }

  fetchTodos() {
    return (dispatch) => {
      // we dispatch an event here so we can have "loading" state.
      dispatch();
      TodoSource.fetch()
        .then((todos) => {
          // we can access other actions within our action through `this.actions`
          this.updateTodos(todos);
        })
        .catch((errorMessage) => {
          this.todosFailed(errorMessage);
        });
      }
  }

  todosFailed(errorMessage) {
    return errorMessage;
  }

  addTodo(todo) {
    return todo;
  }
}

export default alt.createActions(TodoActions);
