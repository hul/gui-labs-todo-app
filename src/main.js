import React from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './components/TodoApp';

import injectTapEventPlugin from "react-tap-event-plugin";

class App extends React.Component {
  render() {
    return (
      <TodoApp />
    );
  }
}

injectTapEventPlugin();
ReactDOM.render(<App />, document.querySelector("#app"));
