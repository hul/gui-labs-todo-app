var mockData = [
  {
    title: "Clean the apartment",
    isCompleted: false
  },
  {
    title: "Do the homework",
    isCompleted: false
  },
  {
    title: "Run 5km",
    isCompleted: false
  }
];

var TodoSource = {
  fetch: function () {
    // returning a Promise because that is what fetch does.
    return new Promise(function (resolve, reject) {
      // simulate an asynchronous action where data is fetched on
      // a remote server somewhere.
      setTimeout(function () {
        // resolve with some mock data
        resolve(mockData);
      }, 250);
    });
  }
};

export default TodoSource
