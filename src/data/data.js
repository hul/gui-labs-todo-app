var TODOS = [
  {
    title: "Clean the apartment",
    isCompleted: false
  },
  {
    title: "Do the homework",
    isCompleted: false
  },
  {
    title: "Run 5km",
    isCompleted: false
  }
];

export default TODOS
