var express = require('express');
var fs = require('fs');

var app = express();
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

app.get('/todos', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  return res.sendFile(__dirname + '/data/todos.json');
});

var server = app.listen(8080, () => {
  var host = server.address().address;
  var port = server.address().port;

  console.log('TODO app is listening at http://%s:%s', host, port);
});
